# EA11R Microsquirt PNP

Tuning and documentation for users of [Kei Sport USA's EA11R PNP](https://www.keisport.us/pnp/).

User documentation is [here](https://www.keisport.us/pnp/doc).

## Files

KeiSportUSA_EA11R_PNP.tsproj is for OEM injectors at 220 kPa of fuel pressure up to 210 kPa of manifold pressure

KeiSportUSA_EA11R_PNP_DW450cc.tsproj is for Deatschwerks 22S-00-0450 injectors at 220 kPa of fuel pressure up to 210 kPa of manifold pressure